<?php

session_start();

//     AUTOLOADING
spl_autoload_register ( function ( $className) {
    $parts = explode('\\', $className);
    $numParts = count($parts);

    $filePath='..'.DIRECTORY_SEPARATOR;
    for($i=1; $i<count($parts)-1; $i++){
        $filePath.=    strtolower($parts[$i]).DIRECTORY_SEPARATOR;
    }
    $filePath.=end($parts) . '.php';
    require $filePath;
});



