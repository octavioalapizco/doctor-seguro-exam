<?php

namespace DoctorSeguro\Examen;

include './config/config.php';
use DoctorSeguro\Examen\Logic\Classes\FileSystem;
use DoctorSeguro\Examen\Logic\Classes\File;
use DoctorSeguro\Examen\Logic\Classes\Folder;
use DoctorSeguro\Examen\Logic\Classes\Resource;
use DoctorSeguro\Examen\Logic\Classes\Root;
use DoctorSeguro\Examen\Logic\FileSystemManager;


$fileSystemManager = new FileSystemManager();
$fileSystem = $fileSystemManager->getFileSystem();

$path ='';
if (isset($_GET['path'])){
    $path =    $_GET['path'];
}

$container = $fileSystem->getFromPath($path);
$breadcrumb = $fileSystem->getBreadcrumb($path);


?>
<html>
<head>
</head>
<body>

<div>
    <a href="index.php">Home / </a>
<?php
    $url='index.php?path=';
    foreach($breadcrumb as $bc){
        $url.='/'.$bc;
        echo '<a href="'.$url.'">'.$bc.' /</a>';
    }

?>
    <br/><br/>
</div>
    <table>
        <thead>
            <tr>
                <td>Name</td>
                <td>Created</td>
                <td>Directory</td>
            </tr>
        </thead>
        <tbody>
        <?php
        /** @var  $resource Resource */
        foreach($container->getContent() as $resource){
            $created = $resource->getCreated()->format('Y-m-d h:i:s');

            $parts = explode('\\', get_class($resource));
            $class=end($parts);
            $isFolder = false;
            if ($class==='Folder'){
                $checkbox='<input type="checkbox" checked disabled >';
                echo '<tr><td> <a href="/index.php?path='.$path.'/'.$resource->getName().'" >'.$resource->getName().'</a></td><td>'.$created.'</td><td>'.$checkbox.'</td></tr>';
            } else {
                $checkbox='<input type="checkbox" disabled >';
                echo '<tr><td>'.$resource->getName().'</td><td>'.$created.'</td><td>'.$checkbox.'</td></tr>';
            }
        }
        ?>
        </tbody>
    </table>
</body>

</html>
