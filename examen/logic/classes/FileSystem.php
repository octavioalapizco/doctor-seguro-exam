<?php 

namespace DoctorSeguro\Examen\Logic\Classes;

use DoctorSeguro\Examen\Logic\Classes\Container;
use DoctorSeguro\Examen\Logic\Classes\Resource;

class FileSystem
{
    /** @var  $home Root */
    private $home;

    /**
     * FileSystem constructor.
     * @param Root $home
     */
    public function __construct()
    {
        $home= new Root('home');
        $this->home = $home;
    }


    /**
     * @return Root
     */
    public function getHome()
    {
        return $this->home;
    }

    /**
     * @param $path string
     * @return \DoctorSeguro\Examen\Logic\Classes\Container
     */
    public function getFromPath($path): Container {

        if ($path=='' || $path=='/'){
            return $this->getHome();
        }else{
            $parts = explode('/', $path);
            array_shift($parts);

            $container=$this->getHome();
            foreach($parts as $p){
                $container=$container->getFolder($p);
            }

            return $container;
        }

    }


    public function getBreadcrumb($path){
       $parts = explode('/', $path);
       array_shift($parts );
       return $parts;

    }

}