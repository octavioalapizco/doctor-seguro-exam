<?php
/**
 * Created by PhpStorm.
 * User: octavio
 * Date: 28/01/18
 * Time: 06:26 PM
 */

namespace DoctorSeguro\Examen\Logic\Classes;


class Folder extends Container
{
    /** @var  $parent Container */
    private $parent;

    /**
     * @return Container
     */
    public function getParent()
    {
        return $this->parent;
    }



}