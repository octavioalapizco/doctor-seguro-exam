<?php

namespace DoctorSeguro\Examen\Logic\Classes;

class Container extends Resource
{
    /** @var  Resource */
    private $content;
    /**
     * @param $resource Resource
     */
    public function addResource($resource){
        $this->content[]=$resource;
    }

    /**
     * @return Resource
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param Resource $content
     * @return Container
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }


    public function getFolder($folderName){
        $content =$this->getContent();

        /** @var  $resource Resource */
        foreach($content as $resource){
            $classNameArr = explode('\\', get_class($resource));

            $class=end($classNameArr);
            $isFolder = false;
            if ($class==='Folder'){
                if ($resource->getName()==$folderName){
                    return $resource;
                }
            }else {
                echo $class;
            }
        }

    }
}