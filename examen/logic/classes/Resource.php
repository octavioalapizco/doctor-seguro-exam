<?php
/**
 * Created by PhpStorm.
 * User: octavio
 * Date: 28/01/18
 * Time: 06:24 PM
 */

namespace DoctorSeguro\Examen\Logic\Classes;


class Resource
{
    private $name;
    private $created;

    /**
     * Resource constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->created= new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Resource
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return Resource
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

}