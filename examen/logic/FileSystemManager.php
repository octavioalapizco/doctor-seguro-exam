<?php
/**
 * Created by PhpStorm.
 * User: octavio
 * Date: 29/01/18
 * Time: 12:39 PM
 */

namespace DoctorSeguro\Examen\Logic;

use DoctorSeguro\Examen\Logic\Classes\FileSystem;
use DoctorSeguro\Examen\Logic\Classes\File;
use DoctorSeguro\Examen\Logic\Classes\Folder;
use DoctorSeguro\Examen\Logic\Classes\Resource;
use DoctorSeguro\Examen\Logic\Classes\Root;

class FileSystemManager
{
    /** @var  FileSystem */
    private $fileSystem;

    function getFileSystem() : FileSystem {

        $imagesFolder = new Folder('images');
        $imagesFolder->addResource(new File('main_logo.png'));
        $imagesFolder->addResource(new File('logo_small.png'));
        $imagesFolder->addResource(new File('icons.png'));

        $src = new Folder('src');
        $src->addResource(new File('index.php'));
        $src->addResource(new File('config.php'));
        $src->addResource(new File('default.php'));

        $tests = new Folder('tests');
        $tests->addResource(new File('test-index.php'));
        $tests->addResource(new File('test-config.php'));
        $tests->addResource(new File('text-default.php'));

        $myProject = new Folder('MyProject');
        $myProject->addResource($imagesFolder);
        $myProject->addResource($src);
        $myProject->addResource($tests);
        $myProject->addResource(new File('README.md'));

        $fileSystem = new FileSystem();
        $root = $fileSystem->getHome();
        $root->addResource($myProject);

        return $fileSystem;
    }

}